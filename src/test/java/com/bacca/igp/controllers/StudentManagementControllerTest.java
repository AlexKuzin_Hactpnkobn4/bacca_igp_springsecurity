package com.bacca.igp.controllers;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;

import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StudentManagementControllerTest {

    @Autowired
    StudentManagementController controller;

    @Autowired
    TestRestTemplate testRestTemplate;
    @LocalServerPort
    private int port;

    @BeforeEach
    void setUp() {
    }

    @Test
    void contextLoads() {
        assertNotNull(controller);
    }

    @Test
    public void greetingShouldReturnDefaultMessage() {
        assertThat(this.testRestTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("Hello, Spring Security Bacca!");
    }

    @Test
    public void basicAuthFailTest(){
        ResponseEntity<String> response = testRestTemplate.exchange("http://localhost:" + port + "/api/v1/main/student/0",
                HttpMethod.GET, null, String.class);
        System.out.println(response.getStatusCodeValue());
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.UNAUTHORIZED.value())
        ;
    }


    @Test
    public void basicAuthSuccessTest(){
        String auth = "ai:1";
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(StandardCharsets.US_ASCII) );
        String authHeader = "Basic " + new String( encodedAuth );

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authHeader);
        ResponseEntity<String> responseWithBasic = testRestTemplate.exchange("http://localhost:" + port + "/api/v1/main/student/0",
                HttpMethod.GET, new HttpEntity<>(headers), String.class);
        assertThat(responseWithBasic.getStatusCodeValue()).isEqualTo(200);
    }


}