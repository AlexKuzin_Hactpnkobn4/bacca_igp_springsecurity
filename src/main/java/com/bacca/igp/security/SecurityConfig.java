package com.bacca.igp.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Component;

import static com.bacca.igp.security.UserPermissions.*;
import static com.bacca.igp.security.UserRoles.*;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Component
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()

                .authorizeHttpRequests()
                .antMatchers("/", "index", "/css/*", "/js/*", "/login", "/logout")
                    .permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/api/v1/main/student/1")
        ;
    }

    @Bean
    protected UserDetailsService userDetailsService(PasswordEncoder encoder) {
        UserDetails nastia = User.builder()
                .username("nastia")
                .password(encoder.encode("1"))
                .authorities(STUDENT.getGrantedAuthorities())
                .build();
        UserDetails ai = User.builder()
                .username("ai")
                .password(encoder.encode("1"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();
        UserDetails tomThum = User.builder()
                .username("tomthum")
                .password(encoder.encode("1"))
                .authorities(ADMIN_TRAINEE.getGrantedAuthorities())
                .build();


        return new InMemoryUserDetailsManager(
                nastia,
                ai,
                tomThum);
    }

    @Bean
    protected PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(10);
    }
}
