package com.bacca.igp.security;

public enum UserPermissions {

    STUDENT_READ ("student:read"),
    STUDENT_WRITE("student:write"),
    COURSE_READ  ("course:read"),
    COURSE_WRITE ("course:write"),
    ;

    private final String name;

    UserPermissions(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
