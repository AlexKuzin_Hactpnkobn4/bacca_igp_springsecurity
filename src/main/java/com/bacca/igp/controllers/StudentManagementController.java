package com.bacca.igp.controllers;

import com.bacca.igp.dto.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("management/api/v0/students")
public class StudentManagementController {

    private final List<Student> STUDENTS = new ArrayList<>(Arrays.asList(
            new Student(0, "Alex"),
            new Student(1, "Julia"),
            new Student(2, "Egor")
    ));

    @GetMapping("/")
    public List<Student> getAllStudents(){
        return STUDENTS;
    }

    @PostMapping("/")
    public void RegisterNewStudent(@RequestBody Student student){
        STUDENTS.add(student);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Integer id){
        STUDENTS.remove(
                STUDENTS.stream()
                        .filter(student -> Objects.equals(student.getStudentId(), id))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException("")
                        )
        );
    }
}
