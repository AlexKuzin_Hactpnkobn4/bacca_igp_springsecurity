package com.bacca.igp.controllers;

import com.bacca.igp.dto.Student;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("api/v1/main")
public class MainController {

    private final List<Student> STUDENTS = Arrays.asList(
      new Student(0, "Alex"),
      new Student(1, "Julia"),
      new Student(2, "Egor")
    );

    @GetMapping("/student/{studentId}")
    @PreAuthorize("hasRole(T(com.bacca.igp.security.UserRoles).STUDENT.name())")
    public Student getStudent(@PathVariable("studentId") Integer studentId){
        return STUDENTS.stream().filter(student -> student.getStudentId().equals(studentId)).findFirst().orElseThrow(NoSuchElementException::new);
    }
}
